$(function() {
    var percents = [];
    var render;
    $(function() {
        var percent = $('#percent')[0];
        var coverSides = $('.cover_side');
        var portionRatio = 1 / 8;
        var HALF_SIZE_PLUS_1 = 51;
        var SIDES = ["Right", "Bottom", "Bottom", "Left", "Left", "Top", "Top", "Right"];

        function clamp(val, min, max) {
            return val < min ? min : (val > max ? max : val);
        }
        render = function(p) {
            var size, ratio, i;
            for (i = 0; i < 8; i++) {
                ratio = clamp(p / 100 - portionRatio * i, 0, portionRatio) / portionRatio;
                size = i % 2 == 0 ? ratio * HALF_SIZE_PLUS_1 : (1 - ratio) * HALF_SIZE_PLUS_1;
                coverSides[i].style["border" + SIDES[i] + "Width"] = size + "px";
            }
            percent.innerHTML = ~~(p) + "%";
        };
    });
    $(function() {
        var percentDrainInterval = window.setInterval(function() {
            if (percents.length === 0) {
                return;
            }
            var percent = percents[0];
            percents = percents.slice(1);
            render(percent);
            if (percent === 100) {
                window.clearInterval(percentDrainInterval);
                $('body').addClass('loaded run-anim');
                scrollock.trigger('scroll');
            }
        }, 40);
    });
    $(function() {
        var images = ['/Themes/EleksCookiesTheme/Content/Digital/img/resize_arrows.gif', 'Themes/EleksCookiesTheme/Content/Digital/apple-touch-icon-precomposed.png', 'Themes/EleksCookiesTheme/Content/Digital/apple-touch-icon.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bug.png', 'Themes/EleksCookiesTheme/Content/Digital/img/logo.png', 'Themes/EleksCookiesTheme/Content/Digital/img/form-sprite.png', 'Themes/EleksCookiesTheme/Content/Digital/img/ie_confeti.png', 'Themes/EleksCookiesTheme/Content/Digital/img/ie_icon.png', 'Themes/EleksCookiesTheme/Content/Digital/img/ipad_content_img.png', 'Themes/EleksCookiesTheme/Content/Digital/img/magnifying_glass.png', 'Themes/EleksCookiesTheme/Content/Digital/img/soc_icons_sprite.png', 'Themes/EleksCookiesTheme/Content/Digital/img/window_wrap_text_zoom.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA1.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA2.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA3.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA4.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA5.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA6.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA7.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ba/BA8.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD1.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD2.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD3.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD4.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD5.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD6.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD7.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD8.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD9.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/design/VD10_bg.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D1.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D2.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D3.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D4.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D5.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D6.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D7.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/dev/D8_bg.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S1.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S2.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S3.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S4.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S5.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S6.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S7.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S8.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S9.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/golive/S10_bg.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T1.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T2.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T3.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T4.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T5.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T6.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T7.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T8.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/qa/T9_bg.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/tech/TC1.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/tech/TC2.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/tech/TC3.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/tech/TC4.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/tech/TC5.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/tech/TC6.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/tech/TC7_bg.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX1.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX2.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX3.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX4.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX5.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX6.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX7.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX8.png', 'Themes/EleksCookiesTheme/Content/Digital/img/bg/ux/UX9_bg.png', 'Themes/EleksCookiesTheme/Content/Digital/img/case/case_01.jpg', 'Themes/EleksCookiesTheme/Content/Digital/img/case/case_02.jpg', 'Themes/EleksCookiesTheme/Content/Digital/img/case/case_03.jpg', 'Themes/EleksCookiesTheme/Content/Digital/img/case/case_04.jpg', 'Themes/EleksCookiesTheme/Content/Digital/img/tablet_portrait.jpg', 'Themes/EleksCookiesTheme/Content/Digital/img/video_bg.jpg', 'Themes/EleksCookiesTheme/Content/Digital/img/promo_ico/bulb.png', 'Themes/EleksCookiesTheme/Content/Digital/img/promo_ico/circle.png', 'Themes/EleksCookiesTheme/Content/Digital/img/promo_ico/closing_brackets.png', 'Themes/EleksCookiesTheme/Content/Digital/img/promo_ico/cross.png', 'Themes/EleksCookiesTheme/Content/Digital/img/promo_ico/dollar.png', 'Themes/EleksCookiesTheme/Content/Digital/img/promo_ico/heart.png', '/Themes/EleksCookiesTheme/Content/Digital/img/promo_ico/user.png'];
        var svgs = [];
        if (Modernizr.svg) {
            images = images.concat(svgs);
        }
        var imageCounter = images.length;

        function progress() {
            return (1 - imageCounter / images.length) * 100;
        }

        function createImage(source) {
            var image = new Image();
            var $image = $(image);
            var completeOne = function() {
                --imageCounter;
                $image.off();
                percents.push(progress());
            };
            $image.on('load error abort', completeOne);
            image.src = source;
        }
        for (var i = 0; i < images.length; i++) {
            createImage(images[i]);
        }
    });
});