(function() {
    var method;
    var noop = function() {};
    var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
    var length = methods.length;
    var console = (window.console = window.console || {});
    while (length--) {
        method = methods[length];
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
var showHome;
$(function() {
    var click;
    if ('ontouchstart' in window) {
        click = 'touchstart';
    } else {
        click = 'click';
    }
    'use strict';
    scrollock.block();
    var $sections = $('.scroll-section'),
        $currentSection = $sections.eq(0),
        index = 0,
        blocked = true,
        timeout = false,
        time = +$currentSection.attr('data-scroll-lock-time-in'),
        quiet = false;
    scrollock.on('scroll', function(e, direction) {
        if (blocked) {
            if (!timeout) {
                timeout = true;
                setTimeout(function() {
                    blocked = false;
                    timeout = false;
                    c.log('time for animations %d is over, you can scroll now', time);
                }, time);
            }
        } else {
            switch (direction) {
                case 'up':
                    showAtIndex(index - 1, 'up');
                    scrollock.trigger('scroll');
                    break;
                case 'down':
                    showAtIndex(index + 1, 'down');
                    scrollock.trigger('scroll');
                    break;
                default:
                    blocked = false;
            }
        }
    });

    function showAtIndex(i, direction) {
        var $nextSection = $sections.eq(i),
            $currentSection = $sections.eq(index),
            outTime = +$currentSection.attr('data-scroll-lock-time-out');
        if (!$nextSection.length || i < 0) {
            c.log('tried to go %s, but not valid index: %d', direction, i);
            return;
        }
        c.log('going %s, delivering page #%d', direction, i + 1);
        $nextSection.addClass(direction === 'down' ? 'goDown' : 'goTop');
        $('body').removeClass('run-anim');
        $currentSection.removeClass('active');
        c.log('removed active class from %d page', index + 1);
        setTimeout(function() {
            c.log('waited %d, so now can do animations', outTime);
            $currentSection.addClass('animate ' + ((direction === 'down') ? 'goTop' : 'goDown')).removeClass('bringToFront');
            $nextSection.addClass('animate bringToFront');
            setActivePagination(i);
            setTimeout(function() {
                c.log('waited for 1000ms, so now removing all extra classes');
                c.log('page at %d has class active, let the animations begin', index + 1);
                $nextSection.addClass('active');
                $('.animate').removeClass('animate');
                $('.goTop').removeClass('goTop');
                $('.goDown').removeClass('goDown');
                $('body').css('background-color', $('section.active').css('background-color'));
            }, 1000);
            index = i;
        }, outTime);
        blocked = true;
        time = +$nextSection.attr('data-scroll-lock-time-in');
    }

    function setActivePagination(i) {
        var all = $('.pagination a'),
            current = all.eq(i);
        all.filter('.active').removeClass('active');
        current.addClass('active');
        c.log('setting %d pagination link as active', i + 1);
    }
    $(function() {
        $('.pagination').on(click, 'li a', function() {
            var children = $('.pagination').children(),
                current = $(this),
                i = children.index(current.parent());
            showAtIndex(i, i > index ? 'down' : 'up');
        });
    });
    $(function() {
        $('.scroll_down').on(click, function() {
            showAtIndex(index + 1, 'down');
        });
    });
    showHome = function showHome() {
        showAtIndex(0, 'up');
    };
    var c = {
        log: function() {
            if (!quiet) {
                console.log.apply(console, arguments);
            }
        }
    }
});
$(function() {
    var click;
    if ('ontouchstart' in window) {
        click = 'touchstart';
    } else {
        click = 'click';
    }
    var $pageWrapper = $('.page-wrapper'),
        $case_study = $('#case_study'),
        $contact_us = $('#contact_us'),
        $currentPushDown;

    function setTransformY($element, value) {
        var pxValue = 'translateY(' + value + 'px)';
        $element.css({
            '-webkit-transform': pxValue,
            '-moz-transform': pxValue,
            '-ms-transform': pxValue,
            '-o-transform': pxValue,
            'transform': pxValue
        });
    }

    function showPushDown($push_down) {
        $push_down.addClass('active');
        $push_down.css('background-color', $('section.active').css('background-color'));
        if (Modernizr.csstransitions) {
            setTransformY($pageWrapper, $push_down.outerHeight(true));
        }
    }

    function hidePushDown($push_down, onComplete) {
        if (Modernizr.csstransitions) {
            setTransformY($pageWrapper, 0);
            var TRANSITION_EVENTS = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
            $pageWrapper.on(TRANSITION_EVENTS, function(e) {
                if (e.target === $pageWrapper[0]) {
                    $push_down.removeClass('active');
                    onComplete && onComplete();
                    $pageWrapper.off(TRANSITION_EVENTS);
                }
            });
        } else {
            $push_down.removeClass('active');
            onComplete && onComplete();
        }
    }

    function onShowOrHide($newPushDown) {
        var stateHandlers = {
            'show': function() {
                showPushDown($newPushDown);
                $currentPushDown = $newPushDown;
                scrollock.quiet(true);
                $('#close_case_study,#close_contact_us').removeClass('hide-btn').addClass('show-btn');
            },
            'hide': function() {
                hidePushDown($newPushDown);
                $currentPushDown = undefined;
                scrollock.quiet(false);
                $('#close_case_study,#close_contact_us').removeClass('show-btn').addClass('hide-btn');
            },
            'switch': function() {
                hidePushDown($currentPushDown, function() {
                    showPushDown($newPushDown);
                });
                $currentPushDown = $newPushDown;
                scrollock.quiet();
            }
        };
        var state;
        if ($currentPushDown) {
            state = $currentPushDown === $newPushDown ? 'hide' : 'switch';
        } else {
            state = 'show';
        }
        stateHandlers[state]();
    }
    $case_study.css('height', $case_study.height() + 'px');
    $contact_us.css('height', $contact_us.height() + 'px');
    $('#show_case_study').on(click, function(e) {
        e.preventDefault();
        onShowOrHide($case_study);
    });
    $('#footer_case_studies').on(click, function(e) {
        e.preventDefault();
        onShowOrHide($case_study);
    });
    $('#close_case_study').on(click, function() {
        onShowOrHide($case_study);
    });
    $('#show_contact_us').on(click, function(e) {
        e.preventDefault();
        onShowOrHide($contact_us);
    });
    $('#footer_contact').on(click, function(e) {
        e.preventDefault();
        onShowOrHide($contact_us);
    });
    $('#close_contact_us').on(click, function() {
        onShowOrHide($contact_us);
    });
    $(function() {
        $('.header_logo').on(click, function(event) {
            event.preventDefault();
            if ($currentPushDown) {
                hidePushDown($currentPushDown, function() {
                    showHome()
                });
                $currentPushDown = undefined;
                scrollock.quiet(false);
            } else {
                showHome();
            }
        });
    });

    function caseStudyResize() {
        var h1 = 580,
            h2 = 380,
            w = 1920;
        var windowHeight = $(window).width() / (w / (h1 + h2));
        var h = windowHeight + 73;
        $('.case_wrapper .dummy_align_block').height(windowHeight / ((h1 + h2) / h2));
        $('.case_wrapper:nth-child(1) .dummy_align_block').height(windowHeight / ((h1 + h2) / h1));
        $case_study.css('height', h + 'px');
        if ($case_study.hasClass('active')) {
            setTransformY($pageWrapper, h)
        };
    }
    $(window).load(function() {
        caseStudyResize();
    });
    $(window).resize(function() {
        caseStudyResize();
    });
    var $control_name = $('#control-name'),
        $control_email = $('#control-email'),
        $control_message = $('#control-message'),
        $control_button = $('#control-button');
    $control_name.change(function() {
        valid($control_name);
    });
    $control_email.change(function() {
        valid($control_email);
    });
    $control_message.change(function() {
        valid($control_message);
    });
    $control_button.click(function() {
        sendMail();
        return false;
    });

    function sendMail() {
        setChanged($control_name);
        setChanged($control_email);
        setChanged($control_message);
        if (validateForm()) {
            var name = $control_name.val();
            var email = $control_email.val();
            var text = $control_message.val();
            $contact_us.addClass('sent');
            window.setTimeout(function() {
                $contact_us.removeClass("sent");
                onShowOrHide($contact_us);
            }, 3000);
            $.ajax({
                type: "POST",
                url: "/digital-send-mail",
                data: "name=" + name + "&email=" + email + "&text=" + text,
                success: function(response) {
                    console.log('Email was sent successfully.');
                },
                error: function(request, error) {
                    console.log('Send email: ' + error);
                }
            });
        }
    }

    function setChanged(control) {
        control.attr('changed', 'true');
    }

    function valid(control) {
        setChanged(control);
        validateForm();
    }

    function validateForm() {
        var allValid = true;
        removeAllClass('error');
        removeAllClass('all_right');
        allValid = validateControl($control_name, 'error-name', requiredValidator);
        allValid &= validateControl($control_email, 'error-email', emailValidator);
        allValid &= validateControl($control_message, 'error-message', requiredValidator);
        setSendButton(allValid);
        return allValid;
    }

    function requiredValidator(value) {
        return value != '';
    }

    function emailValidator(value) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return value != '' && re.test(value);
    }

    function setSendButton(allValid) {
        if (!allValid) {
            $control_button.addClass('error');
        } else {
            $control_button.addClass('all_right');
        }
    }

    function validateControl(control, errorId, validator) {
        if (control.attr('changed') != 'true') {
            return false;
        }
        var value = control.val();
        if (!validator(value)) {
            control.addClass('error');
            $('#' + errorId).removeClass('hidden');
            return false;
        } else {
            control.addClass('all_right');
            $('#' + errorId).addClass('hidden');
            return true;
        }
    }

    function removeAllClass(className) {
        $control_name.removeClass(className);
        $control_email.removeClass(className);
        $control_message.removeClass(className);
        $control_button.removeClass(className);
    }
});
(function(global) {
    'use strict';
    var Scrollock = function() {
        var events = {
                scroll: function() {},
                scrollUp: function() {},
                scrollDown: function() {},
                wheel: function() {},
                wheelUp: function() {},
                wheelDown: function() {},
                keypress: function() {},
                keyUp: function() {},
                keyDown: function() {},
                swipe: function() {},
                swipeUp: function() {},
                swipeDown: function() {}
            },
            instance = this,
            blocked = false,
            quiet = false,
            swipeStartX, swipeStartY, swipeStartTime;

        function preventDefault(e) {
            e = e || window.event;
            if (e.preventDefault) {
                e.preventDefault();
            }
            e.returnValue = false;
        }

        function inArray(element, array) {
            if (typeof Array.prototype.indexOf === 'function') {
                return array.indexOf(element) !== -1;
            }
            var n = array.length,
                i = 0;
            for (; i < n; i++) {
                if (element === array[i]) {
                    return true;
                }
            }
            return false;
        }

        function keypress(e) {
            var keys = [33, 34, 35, 36, 37, 38, 39, 40],
                keysUp = [33, 36, 38],
                keysDown = [34, 35, 40],
                direction;
            e = e || window.event;
            if (blocked && inArray(e.keyCode, keys)) {
                preventDefault(e);
            }
            if (inArray(e.keyCode, keys)) {
                if (inArray(e.keyCode, keysUp)) {
                    direction = 'up';
                    trigger('keyUp', e, direction);
                    trigger('scrollUp', e, direction);
                } else {
                    direction = 'down';
                    trigger('keyDown', e, direction);
                    trigger('scrollDown', e, direction);
                }
                trigger('keypress', e, direction);
                trigger('scroll', e, direction);
            }
        }

        function wheel(e) {
            var direction;
            if (blocked) {
                preventDefault(e);
            }
            e = e || window.event;
            if (getDelta(e) < 0) {
                direction = 'down';
                trigger('wheelDown', e, direction);
                trigger('scrollDown', e, direction);
            } else {
                direction = 'up';
                trigger('wheelUp', e, direction);
                trigger('scrollUp', e, direction);
            }
            trigger('wheel', e, direction);
            trigger('scroll', e, direction);
        }

        function swipe(e) {
            var threshold = 20,
                allowedTime = 20,
                direction, changeX, changeY, changeTime;
            if (e.type === 'touchmove') {
                e.preventDefault();
            }
            if (e.type === 'touchstart') {
                swipeStartX = e.changedTouches[0].pageX;
                swipeStartY = e.changedTouches[0].pageY;
                swipeStartTime = +new Date();
                return;
            }
            if (e.type === 'touchend') {
                changeX = swipeStartX - e.changedTouches[0].pageX;
                changeY = swipeStartY - e.changedTouches[0].pageY;
                changeTime = swipeStartTime - +new Date();
            }
            if (Math.abs(changeX) <= 100 && changeTime < allowedTime && Math.abs(changeY) >= threshold) {
                console.log(changeY)
                if (changeY > 0) {
                    direction = 'down';
                    trigger('swipeDown', e, direction);
                    trigger('scrollDown', e, direction);
                } else {
                    trigger('swipeUp', e, direction);
                    trigger('scrollUp', e, direction);
                    direction = 'up';
                }
                trigger('swipe', e, direction);
                trigger('scroll', e, direction);
            }
        }

        function block(what) {
            blocked = true;
            return this;
        }

        function unblock(what) {
            blocked = false;
            return this;
        }

        function getDelta(e) {
            if (e.wheelDelta) {
                return e.wheelDelta;
            }
            if (e.detail) {
                return e.detail * -40;
            }
            if (e.originalEvent.detail) {
                return e.originalEvent.detail * -40;
            }
            if (e.originalEvent && e.originalEvent.wheelDelta) {
                return e.originalEvent.wheelDelta;
            }
        }

        function on(event, handler) {
            if (events.hasOwnProperty(event) && typeof handler === 'function') {
                events[event] = handler;
            }
            return this;
        }

        function off(event) {
            if (events.hasOwnProperty(event)) {
                events[event] = function() {};
            }
            return this;
        }

        function trigger(event) {
            if (!quiet && events.hasOwnProperty(event)) {
                events[event].apply(instance, Array.prototype.slice.call(arguments, 1));
            }
        }

        function setQuiet(bool) {
            if (typeof bool === 'undefined') {
                quiet = !quiet;
            }
            if (bool === !!bool) {
                quiet = bool;
            }
            return quiet;
        }
        if (window.addEventListener) {
            window.addEventListener('DOMMouseScroll', wheel, false);
        }
        window.onmousewheel = document.onmousewheel = wheel;
        document.onkeydown = keypress;
        document.addEventListener('touchstart', swipe, false);
        document.addEventListener('touchmove', swipe, false);
        document.addEventListener('touchend', swipe, false);
        return {
            on: on,
            off: off,
            block: block,
            unblock: unblock,
            trigger: trigger,
            quiet: setQuiet
        };
    };
    global.scrollock = new Scrollock();
})(this);